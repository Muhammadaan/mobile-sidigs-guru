import 'package:flutter/material.dart';

// const String urlServer = "https://sidigs.com/api/teacher";
const String urlServer = "https://demo.sidigs.com/api/teacher";
const Color mainColor = Color(0xFF0071F2);
const Color colorRed = Colors.red;

//
String imgLogo = 'assets/img/logo.png';
String imgLogoText = 'assets/img/logotext.png';
String imgBgHome = 'assets/img/bg_home.png';
String imgFotoSiswa = 'assets/img/foto_siswa.jpeg';
String imgFotoGuru = 'assets/img/profile.jpg';
String iconMenuPresensi = 'assets/img/presensi.png';
String iconMenuVc = 'assets/img/vc.png';
String iconMenuModul = 'assets/img/ic_menu_modul.png';
String iconMenuGaji = 'assets/img/ic_menu_gaji.png';
String iconMenuJadwal = 'assets/img/ic_menu_jadwal.png';

String iconMenuLainnya = 'assets/img/ic_menu_lainnya.png';
String iconMenuTahfidz = 'assets/img/ic_menu_tahfidz.png';
String iconMenuDiskusi = 'assets/img/ic_menu_diskusi.png';
String iconMenuJurnal = 'assets/img/ic_menu_jurnal.png';
String iconMenuDiagram = 'assets/img/ic_menu_diagram.png';
String iconMenuRaport = 'assets/img/ic_menu_raport.png';
String iconMenuRpp = 'assets/img/ic_menu_rpp.png';
