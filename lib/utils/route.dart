import 'package:flutter/material.dart';
import 'package:sidigsguru/screens/login_screen/login_screen.dart';
import 'package:sidigsguru/screens/menu_bottom/menu_bottom.dart';
import 'package:sidigsguru/screens/splash_screen/splash_screen.dart';

class AppRoute {
  static Route<dynamic> generateRoute(RouteSettings setting) {
    switch (setting.name) {
      case '/':
        return MaterialPageRoute(
            settings: setting,
            builder: (BuildContext context) => SplashScreen());

      case '/login':
        return MaterialPageRoute(
            settings: setting,
            builder: (BuildContext context) => LoginScreen());
      case '/menu':
        return MaterialPageRoute(
            settings: setting,
            builder: (BuildContext context) => MenuBottom());

      default:
        return null;
    }
  }
}
