

import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:sidigsguru/utils/app_loading.dart';

List<SingleChildWidget> appprovider = <SingleChildWidget>[
  Provider<AppLoadingProvider>(create: (_) => AppLoadingProvider()),
  // ChangeNotifierProvider<LoginProvider>(
  //     create: (BuildContext context) => LoginProvider(context.read<DataApi>())),
];
