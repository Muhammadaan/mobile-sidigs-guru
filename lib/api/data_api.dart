import 'dart:io';

import 'package:dio/dio.dart';
import 'package:sidigsguru/utils/contant.dart';

class ApiData {
  static Dio dio = Dio();
  static Response response;

  // LOGIN
  static Future login({String username, String password}) async {
    try {
      response = await dio.post(urlServer + '/login', data: {
        "username": username,
        "password": password,
        "role": "teacher"
      });

      return response;
    } on DioError catch (e) {
      if (e.response.statusCode != 200) {
        print(e.toString());
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }
  // LOGIN

  // getAttendanceToday
  static Future getAttendanceToday({String token}) async {
    try {
      response = await dio.get(urlServer + "/attendance/get-today",
          options: Options(headers: {
            'Authorization': 'Bearer $token}',
          }));
      return response;
    } on DioError catch (e) {
      if (e.response.statusCode != 200) {
        print(e.toString());
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }

  // attendanceSubmitToday
  static Future attendanceSubmitToday(
      {String token,
      String evidence,
      String type,
      String notes,
      String latitude,
      String longitude}) async {
    try {
      var formData1 = {
        'latitude': latitude,
        'longitude': longitude,
        'evidence': evidence,
        'type': type,
        'notes': notes,
      };
      response = await dio.post(urlServer + '/attendance/submit-today-base64',
          data: formData1,
          options: Options(headers: {
            'Authorization': 'Bearer $token}',
          }));

      return response;
    } on DioError catch (e) {
      if (e.response.statusCode != 200) {
        print(e.toString());
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }

  // getAttendanceToday
  static Future getRefreshtoken({String token}) async {
    try {
      response = await dio.get(urlServer + "/refresh-token",
          options: Options(headers: {
            'Authorization': 'Bearer $token}',
          }));
      return response;
    } on DioError catch (e) {
      if (e.response.statusCode != 200) {
        print(e.toString());
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }

  // getAttendanceToday
  static Future getInformasiAkun({String token}) async {
    try {
      response = await dio.get(urlServer + "/profile",
          options: Options(headers: {
            'Authorization': 'Bearer $token}',
          }));
      return response;
    } on DioError catch (e) {
      if (e.response.statusCode != 200) {
        print(e.toString());
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }

  static Future gantiKataSandi({
    String token,
    String old_password,
    String password,
  }) async {
    try {
      var formData1 = {"old_password": old_password, "password": password};
      response = await dio.post(urlServer + '/change-pass',
          data: formData1,
          options: Options(headers: {
            'Authorization': 'Bearer $token}',
          }));

      return response;
    } on DioError catch (e) {
      if (e.response.statusCode != 200) {
        print(e.toString());
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }

  static Future getInformasiSekolah({String token}) async {
    try {
      response = await dio.get(urlServer + "/school-profile",
          options: Options(headers: {
            'Authorization': 'Bearer $token}',
          }));
      return response;
    } on DioError catch (e) {
      if (e.response.statusCode != 200) {
        print(e.toString());
      } else {
        print(e.request);
        print(e.message);
      }
    }
  }
}
