import 'package:flutter/material.dart';
import 'package:sidigsguru/screens/chat_screen/chat_screen.dart';
import 'package:sidigsguru/screens/home_screen/home_screen.dart';
import 'package:sidigsguru/screens/profile_screen/profile_screen.dart';
import 'package:sidigsguru/screens/score_screen/score_screen.dart';
import 'package:sidigsguru/utils/contant.dart';
import './menu_bottom_view_model.dart';

class MenuBottomView extends MenuBottomViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      body: IndexedStack(
        index: selectedIndex,
        children: [HomeScreen(), ScoreScreen(), ChatScreen(), ProfileScreen()],
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
          BottomNavigationBarItem(icon: Icon(Icons.list), label: "Nilai"),
          BottomNavigationBarItem(icon: Icon(Icons.message), label: "Chat"),
          BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile")
        ],
        currentIndex: selectedIndex,
        onTap: (index) {
          print("index $index");
          setState(() {
            selectedIndex = index;
          });
        },
        unselectedLabelStyle: TextStyle(color: Colors.grey[300]),
        selectedItemColor: mainColor,
        unselectedItemColor: Colors.grey[600],
        showUnselectedLabels: true,
      ),
    );
  }
}
