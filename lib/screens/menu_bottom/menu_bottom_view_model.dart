import 'package:flutter/material.dart';
import './menu_bottom.dart';

abstract class MenuBottomViewModel extends State<MenuBottom> {
  // Add your state and logic here

 int selectedIndex;
  @override
  void initState() {
    super.initState();
    selectedIndex = this.widget.indexPage;
  }
}
