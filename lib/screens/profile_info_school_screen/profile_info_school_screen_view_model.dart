import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidigsguru/api/data_api.dart';
import 'package:url_launcher/url_launcher.dart';
import './profile_info_school_screen.dart';

abstract class ProfileInfoSchoolScreenViewModel
    extends State<ProfileInfoSchoolScreen> {
  // Add your state and logic here

  bool isLoading = true;
  var dataInformasiSekolah;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    final sp = await SharedPreferences.getInstance();

    var data = await ApiData.getInformasiSekolah(token: sp.getString('token'));
    var jsonObject = json.decode(data.toString());
    dataInformasiSekolah = jsonObject;

    isLoading = false;
    setState(() {});
  }
}
