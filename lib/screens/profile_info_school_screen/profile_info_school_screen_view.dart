import 'package:flutter/material.dart';
import 'package:sidigsguru/widgets/loading_custom.dart';
import './profile_info_school_screen_view_model.dart';

class ProfileInfoSchoolScreenView extends ProfileInfoSchoolScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Informasi Sekolah"),
      ),
      body: isLoading
          ? LoadingCustom()
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    infoAkun(
                        label: "Nama Sekolah",
                        val: dataInformasiSekolah['data']['name'] ?? "-"),
                    infoAkun(
                        label: "Nomer Induk / NPSN",
                        val: dataInformasiSekolah['data']
                                ['registration_number'] ??
                            "-"),
                    infoAkun(
                        label: "Akreditasi",
                        val: dataInformasiSekolah['data']['accreditation'] ??
                            "-"),
                    infoAkun(
                        label: "Telepon",
                        val: dataInformasiSekolah['data']['phone'] ?? "-"),
                    infoAkun(
                        label: "Email",
                        val: dataInformasiSekolah['data']['email'] ?? "-"),
                    infoAkun(
                        label: "Website",
                        val: dataInformasiSekolah['data']['website'] ?? "-"),
                    infoAkun(
                        label: "Facebook",
                        val: dataInformasiSekolah['data']['facebook'] ?? "-"),
                    infoAkun(
                        label: "Instagram",
                        val: dataInformasiSekolah['data']['instagram'] ?? "-"),
                    infoAkun(
                        label: "Youtube",
                        val: dataInformasiSekolah['data']['youtube'] ?? "-"),
                    infoAkun(label: "Tanggal Bergabung", val: "-"),
                  ],
                ),
              ),
            ),
    );
  }

  Widget infoAkun({String label, String val}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          SizedBox(
            height: 5,
          ),
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(10)),
            child: Text(
              val,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
