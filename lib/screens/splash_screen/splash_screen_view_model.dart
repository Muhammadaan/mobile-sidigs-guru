import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './splash_screen.dart';

abstract class SplashScreenViewModel extends State<SplashScreen> {
  // Add your state and logic here
  bool status = false;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    final SharedPreferences prefs = await _prefs;

    if (prefs.getBool('isLogin') != null) {
      status = prefs.getBool('isLogin');
    }

    if (status) {
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pushReplacementNamed(context, '/menu');
      });
    } else {
      Future.delayed(Duration(seconds: 2), () {
        Navigator.pushReplacementNamed(context, '/login');
      });
    }
  }
}
