import 'package:flutter/material.dart';
import 'package:sidigsguru/utils/contant.dart';
import './splash_screen_view_model.dart';

class SplashScreenView extends SplashScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      body: Container(
        child: Container(
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [Image.asset(imgLogo), Image.asset(imgLogoText)],
          ),
        ),
      ),
    );
  }
}
