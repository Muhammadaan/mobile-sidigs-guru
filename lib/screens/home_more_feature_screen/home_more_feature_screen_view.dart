
import 'package:flutter/material.dart';
import 'package:sidigsguru/utils/contant.dart';
import './home_more_feature_screen_view_model.dart';

class HomeMoreFeatureScreenView extends HomeMoreFeatureScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Lainnya"),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            children: [
              menu(name: "Presensi", ic: iconMenuPresensi),
              menu(name: "Modul",ic: iconMenuModul),
              menu(name: "Jurnal",ic: iconMenuJurnal),
              menu(name: "RPP",ic: iconMenuRpp),
              menu(name: "Media Pembelajaran",ic: iconMenuVc),
              menu(name: "Diskusi Mapel",ic: iconMenuDiskusi),
              menu(name: "Jadwal Mengajar",ic: iconMenuPresensi),
              menu(name: "Gaji",ic: iconMenuGaji),
              menu(name: "Tahfidz",ic: iconMenuTahfidz),
            
              menu(name: "Raport",ic: iconMenuRaport),
              menu(name: "Analisa Nilai",ic: iconMenuDiagram),
            ],
          ),
        ),
      ),
    );
  }

  Widget menu({String name, String ic}) {
    return Container(
      margin: EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Container(
            child: Row(
              children: [
                Image.asset(
                  ic,
                  width: 55,
                ),
                SizedBox(
                  width: 15,
                ),
                Text(name),
                Spacer(),
                Icon(
                  Icons.arrow_forward_ios,
                  color: Colors.grey,
                  size: 20,
                )
              ],
            ),
          ),
          SizedBox(height: 10),
          Container(
            color: Colors.grey[200],
            width: double.infinity,
            height: 2,
          )
        ],
      ),
    );
  }
}
