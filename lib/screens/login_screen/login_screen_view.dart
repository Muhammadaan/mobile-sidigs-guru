import 'package:flutter/material.dart';
import 'package:sidigsguru/utils/contant.dart';
import 'package:sidigsguru/widgets/button_text_custom.dart';
import './login_screen_view_model.dart';

class LoginScreenView extends LoginScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: height * 0.15,
                ),
                Center(child: Image.asset(imgLogo)),
                Center(child: Image.asset(imgLogoText)),
                SizedBox(
                  height: height * 0.15,
                ),
                Text("Username"),
                SizedBox(
                  height: 5,
                ),
                TextFormField(
                  controller: userCtrl,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey)),
                      hintText: 'Username',
                      prefixIcon: Icon(
                        Icons.person,
                        color: Colors.grey,
                      ),
                      suffixStyle: TextStyle(color: Colors.grey)),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Username Wajib diisi';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                Text("Password"),
                SizedBox(
                  height: 5,
                ),
                TextFormField(
                  controller: passCtrl,
                  obscureText: isHidePassword,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.grey)),
                      hintText: 'Password',
                      isDense: true,
                      prefixIcon: Icon(
                        Icons.vpn_key,
                        color: Colors.grey,
                      ),
                      suffixIcon: InkWell(
                        onTap: () {
                          togglePasswordVisibility();
                        },
                        child: Icon(
                          isHidePassword
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Colors.grey,
                        ),
                      )),
                  // decoration: InputDecoration(
                  //     border: OutlineInputBorder(
                  //         borderSide: BorderSide(color: Colors.grey)),
                  //     hintText: 'Password',
                  //     prefixIcon: Icon(
                  //       Icons.vpn_key,
                  //       color: Colors.grey,
                  //     ),
                  //     suffixIcon: Icon(
                  //       Icons.visibility,
                  //       color: Colors.grey,
                  //     ),
                  //     suffixStyle: TextStyle(color: Colors.grey)),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Password Wajib diisi';
                    }
                    return null;
                  },
                ),
                SizedBox(
                  height: height * 0.1,
                ),
                ButtonTextCustom(
                  label: "Login",
                  clr: mainColor,
                  isEnable: true,
                  press: () {
                    tapLogin();
                  },
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Dengan masuk atau mendaftar, anda menyetujui Ketentuan Layanan dan Kebijakan Privasi.",
                  textAlign: TextAlign.center,
                ),
                // Row(
                //   mainAxisAlignment: MainAxisAlignment.center,
                //   children: [
                //     Text("Lupa Password"),
                //     TextButton(onPressed: () {}, child: Text("Klik Disini"))
                //   ],
                // )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
