import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidigsguru/api/data_api.dart';
import 'package:sidigsguru/utils/app_loading.dart';
import './login_screen.dart';

abstract class LoginScreenViewModel extends State<LoginScreen> {
  // Add your state and logic here

  TextEditingController userCtrl = TextEditingController();
  TextEditingController passCtrl = TextEditingController();
  bool isHidePassword = true;
  final formKey = GlobalKey<FormState>();
  tapLogin() async {
    final sp = await SharedPreferences.getInstance();
    if (formKey.currentState.validate()) {
      AppLoadingProvider.show(context);

      var data =
          await ApiData.login(password: passCtrl.text, username: userCtrl.text);

      var jsonObject = json.decode(data.toString());

      if (jsonObject['success'] == false) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: Duration(seconds: 3),
            content: const Text('Username / Passwors Anda Salah'),
            backgroundColor: Colors.red,
          ),
        );

        userCtrl.clear();
        passCtrl.clear();

        AppLoadingProvider.hide(context);
      } else {
        print(jsonObject['data']['user']['username']);
        final dataUser = json.encode({
          "username": jsonObject['data']['user']['username'],
          "id": jsonObject['data']['user']['id'],
          "nameschool": jsonObject['data']['school']['name'],
          "nameteacher": jsonObject['data']['user']['teacher']['name'],
        });
        await sp.setString('token', jsonObject['data']['access_token']);
        await sp.setString('dataUser', dataUser);

        await sp.setBool('isLogin', true);

        AppLoadingProvider.hide(context);
        Navigator.pushReplacementNamed(context, '/menu');
      }
    }
  }
  void togglePasswordVisibility() {
    setState(() {
      isHidePassword = !isHidePassword;
    });
  }
}
