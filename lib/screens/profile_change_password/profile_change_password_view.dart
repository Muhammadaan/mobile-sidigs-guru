import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:sidigsguru/widgets/button_text_custom.dart';
import 'package:sidigsguru/widgets/loading_custom.dart';
import './profile_change_password_view_model.dart';

class ProfileChangePasswordView extends ProfileChangePasswordViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Ubah Kata Sandi"),
      ),
      body: isLoading
          ? LoadingCustom()
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    inputText(
                        label: "Kata Sandi Lama", ctrl: kataSandiLamaCtrl),
                    inputText(
                        label: "Kata Sandi Baru", ctrl: kataSandiBaruCtrl),
                    inputText(
                        label: "Konfirmasi Kata Sandi Baru",
                        ctrl: konfirmasiKataSandiBaruCtrl),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 8),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 20,
                            child: Checkbox(
                                value: isTampil,
                                onChanged: (val) {
                                  tampikanSandi(val);
                                }),
                          ),
                          SizedBox(
                            width: 5,
                          ),
                          Text("Tampikan Sandi")
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      child: ButtonTextCustom(
                        isEnable: true,
                        label: "Ubah Kata Sandi",
                        press: () {
                          simpanUbahKataSandi();
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
    );
  }

  Widget inputText({
    String label,
    TextEditingController ctrl,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          SizedBox(
            height: 5,
          ),
          Container(
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(10)),
            child: TextField(
                controller: ctrl,
                obscureText: !isTampil,
                decoration: InputDecoration(
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding: EdgeInsets.all(10),
                    suffixStyle: TextStyle(color: Colors.grey))),
          ),
        ],
      ),
    );
  }
}
