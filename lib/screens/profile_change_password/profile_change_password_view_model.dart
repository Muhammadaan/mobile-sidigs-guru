import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidigsguru/api/data_api.dart';
import './profile_change_password.dart';

abstract class ProfileChangePasswordViewModel
    extends State<ProfileChangePassword> {
  // Add your state and logic here

  bool isTampil = false;
  bool isLoading = false;
  TextEditingController kataSandiLamaCtrl = TextEditingController();
  TextEditingController kataSandiBaruCtrl = TextEditingController();
  TextEditingController konfirmasiKataSandiBaruCtrl = TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  tampikanSandi(bool val) {
    setState(() {
      isTampil = val;
    });
  }

  simpanUbahKataSandi() async {
    if (kataSandiBaruCtrl.text == konfirmasiKataSandiBaruCtrl.text) {
      setState(() {
        isLoading = true;
      });

      final sp = await SharedPreferences.getInstance();
      var data = await ApiData.gantiKataSandi(
          token: sp.getString('token'),
          old_password: kataSandiLamaCtrl.text,
          password: kataSandiBaruCtrl.text);

      setState(() {
        isLoading = false;
      });
      var jsonObject = json.decode(data.toString());

      if (jsonObject['success']) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: Duration(seconds: 3),
            content: const Text('Kata Sandi Berhasil Di Perbaruhi'),
            backgroundColor: Colors.green,
          ),
        );
        Navigator.pop(context);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: Duration(seconds: 3),
            content: const Text('Kata Sandi Lama Salah'),
            backgroundColor: Colors.red,
          ),
        );
      }
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          duration: Duration(seconds: 3),
          content: const Text('Konfirmasi Kata Sandi Baru Tidak Sama'),
          backgroundColor: Colors.red,
        ),
      );
    }
  }
}
