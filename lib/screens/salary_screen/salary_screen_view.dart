import 'package:flutter/material.dart';
import 'package:sidigsguru/screens/salary_detail_screen/salary_detail_screen.dart';
import 'package:sidigsguru/utils/contant.dart';
import './salary_screen_view_model.dart';

class SalaryScreenView extends SalaryScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Gaji"),
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              listGaji(),
              listGaji(),
              listGaji(),
              listGaji(),
              listGaji(),
              listGaji(),
              listGaji(),
              listGaji(),
              listGaji(),
              listGaji()
            ],
          ),
        ),
      ),
    );
  }

  Widget listGaji() {
    return GestureDetector(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SalaryDetailScreen()),
        );
      },
      child: Container(
        padding: EdgeInsets.all(20),
        margin: EdgeInsets.only(top: 5),
        decoration: BoxDecoration(color: Colors.white),
        child: Row(
          children: [
            Image.asset(
              iconMenuGaji,
              width: 50,
            ),
            SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "IDR 5.000.000",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
                Text("Senin, 10 Maret 2021")
              ],
            ),
            Spacer(),
            Container(
              padding: EdgeInsets.all(5),
              decoration: BoxDecoration(
                  color: Colors.green, borderRadius: BorderRadius.circular(5)),
              child: Text(
                "Confirmed",
                style: TextStyle(fontSize: 10, color: Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }
}
