import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidigsguru/api/data_api.dart';
import './profile_info_screen.dart';

abstract class ProfileInfoScreenViewModel extends State<ProfileInfoScreen> {
  // Add your state and logic here
  bool isLoading = true;
  var dataInformasiAkun;
  String namaGuru = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  getData() async {
    final sp = await SharedPreferences.getInstance();

    var data = await ApiData.getInformasiAkun(token: sp.getString('token'));
    var jsonObject = json.decode(data.toString());
    dataInformasiAkun = jsonObject;

    final dataUser =
        json.decode(sp.getString('dataUser')) as Map<String, dynamic>;

    namaGuru = dataUser['nameteacher'];

    isLoading = false;
    setState(() {});
  }
}
