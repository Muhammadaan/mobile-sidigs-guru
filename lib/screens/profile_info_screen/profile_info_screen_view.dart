import 'package:flutter/material.dart';
import 'package:sidigsguru/widgets/loading_custom.dart';
import './profile_info_screen_view_model.dart';

class ProfileInfoScreenView extends ProfileInfoScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Informasi Akun"),
      ),
      body: isLoading
          ? LoadingCustom()
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    infoAkun(label: "Nama Lengkap", val: namaGuru),
                    infoAkun(
                        label: "Username",
                        val: dataInformasiAkun['data']['user']['username'] ??
                            "-"),
                    infoAkun(
                        label: "Email",
                        val: dataInformasiAkun['data']['user']['email'] ?? "-"),
                    infoAkun(
                        label: "Jabatan",
                        val: dataInformasiAkun['data']['employee_status'] ??
                            "-"),
                    infoAkun(
                        label: "NIP",
                        val: dataInformasiAkun['data']['nip'] ?? "-"),
                    infoAkun(
                        label: "NUPTK",
                        val: dataInformasiAkun['data']['nuptk'] ?? "-"),
                    infoAkun(
                        label: "NPWP",
                        val: dataInformasiAkun['data']['npwp'] ?? "-"),
                    infoAkun(
                        label: "Agama",
                        val: dataInformasiAkun['data']['religion'] ?? "-"),
                    infoAkun(
                        label: "Jenis Kelamin",
                        val: dataInformasiAkun['data']['gender'] ?? "-"),
                    infoAkun(
                        label: "Alamat Sesuai KTP",
                        val: dataInformasiAkun['data']['address'] ?? "-"),
                    infoAkun(
                        label: "Tempat,Tanggal Lahir",
                        val:
                            " ${dataInformasiAkun['data']['birthplace']} ${dataInformasiAkun['data']['birthdate']}"),
                    infoAkun(
                        label: "Tugas Tambahan",
                        val: dataInformasiAkun['data']['additional_task'] ??
                            "-"),
                    infoAkun(
                        label: "Status Perkawinan",
                        val: dataInformasiAkun['data']['marriage_status'] ??
                            "-"),
                    infoAkun(
                        label: "Status Karyawan ",
                        val: dataInformasiAkun['data']['employee_status'] ??
                            "-"),
                  ],
                ),
              ),
            ),
    );
  }

  Widget infoAkun({String label, String val}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(label),
          SizedBox(
            height: 5,
          ),
          Container(
            width: double.infinity,
            padding: EdgeInsets.all(15),
            decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(10)),
            child: Text(
              val,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
