import 'package:flutter/material.dart';
import 'package:sidigsguru/utils/contant.dart';
import 'package:sidigsguru/widgets/button_text_custom.dart';
import 'package:sidigsguru/widgets/loading_custom.dart';
import './presence_screen_view_model.dart';

class PresenceScreenView extends PresenceScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Presensi"),
      ),
      body: isLoad
          ? LoadingCustom()
          : SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(15),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(20),
                      child: Center(
                          child: Text(
                        formattedDate,
                        style:
                            TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                      )),
                    ),
                    Container(
                      child: TextField(
                        maxLines: 4,
                        decoration: InputDecoration(
                          hintText: "Tambahkan Catatan",
                          contentPadding: EdgeInsets.all(15),
                          border: OutlineInputBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    ButtonTextCustom(
                      isEnable: isAbsenMasuk,
                      label: "Absen Masuk",
                      clr: mainColor,
                      press: () {
                        getAbsen(isAbsen: 'masuk');
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    ButtonTextCustom(
                      isEnable: isAbsenPulang,
                      label: "Absen Pulang",
                      clr: mainColor,
                      press: () {
                        getAbsen(isAbsen: 'pulang');
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          "Histori Absensi",
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.normal),
                        ),
                        // InkWell(
                        //   child: Text(
                        //     "Lihat Semua",
                        //     style: TextStyle(
                        //       color: mainColor,
                        //     ),
                        //   ),
                        // )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      child: listHistoriToday.length == 0
                          ? Padding(
                              padding: const EdgeInsets.all(10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(Icons.info_outline),
                                  Text(" Hari ini Anda Belum Absensi"),
                                ],
                              ),
                            )
                          : Column(
                              children: listHistoriToday.map((e) {
                                return cardHistori(
                                  type: e['type'],
                                  date: e['date'],
                                  time: e['time'],
                                );
                              }).toList(),
                            ),
                    )
                  ],
                ),
              ),
          ),
    );
  }

  Container cardHistori({String type, String date, String time}) {
    if (type == "checkin") {
      type = "Masuk";
    } else if (type == "checkout") {
      type = "Pulang";
    }
    var listDate = date.split('-');
    return Container(
      margin: EdgeInsets.only(bottom: 5),
      child: Column(
        children: [
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Absen $type",
                      style: TextStyle(
                          color: mainColor,
                          fontWeight: FontWeight.w600,
                          fontSize: 16),
                    ),
                    Text(listDate[2] + '-' + listDate[1] + '-' + listDate[0])
                  ],
                ),
                Text(
                  time,
                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                )
              ],
            ),
          ),
          Divider(
            thickness: 1,
          )
        ],
      ),
    );
  }
}
