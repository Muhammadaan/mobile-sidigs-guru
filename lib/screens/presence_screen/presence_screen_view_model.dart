import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidigsguru/api/data_api.dart';
import 'package:sidigsguru/screens/presence_submit_data/presence_submit_data.dart';
import './presence_screen.dart';

abstract class PresenceScreenViewModel extends State<PresenceScreen> {
  // Add your state and logic here
  bool isLoad = true;
  DateTime nowDate = DateTime.now().toLocal();
  String formattedDate = DateFormat('EEEE d MMMM y').format(DateTime.now().toLocal());
  List listHistoriToday = [];
  bool isAbsenMasuk = false;
  bool isAbsenPulang = false;
  //  bool isAbsenMasuk = true;
  // bool isAbsenPulang = true;

  initState() {
    super.initState();
    getData();
  }

  getData() async {
    final sp = await SharedPreferences.getInstance();
    print("Token Sekarang");
    print(sp.getString('token'));
    var data = await ApiData.getAttendanceToday(token: sp.getString('token'));
    var jsonObject = json.decode(data.toString());
    listHistoriToday.addAll(jsonObject['data']);
    if (listHistoriToday.length == 0) {
      isAbsenMasuk = true;
    }else if(listHistoriToday.length ==1) {
      isAbsenPulang = true;
    }
    setState(() {
      isLoad = false;
    });
  }

  getLokasi() async {
    var position = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
    var lastPostition = await Geolocator.getLastKnownPosition();

    print(lastPostition);
    print("------");
    print(position);
  }

  getAbsen({String isAbsen}) {
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => PresenceSubmitData(
                title: isAbsen,
              )),
    );
  }
}
