import 'package:flutter/material.dart';
import 'package:sidigsguru/widgets/wait_update_screen.dart';
import './score_screen_view_model.dart';

class ScoreScreenView extends ScoreScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Nilai"),
      ),
      body: isDevelop
          ? WaitUpdateScreen()
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    listNIlai(),
                    listNIlai(),
                    listNIlai(),
                    listNIlai(),
                    listNIlai(),
                    listNIlai(),
                    listNIlai(),
                    listNIlai()
                  ],
                ),
              ),
            ),
    );
  }

  Widget listNIlai() {
    return Container(
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.only(top: 5),
      decoration: BoxDecoration(color: Colors.white),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Muhammad Ainul",
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
              ),
              Text(
                "Senin,12 Juli 2021",
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
              Text("Selesai",
                  style: TextStyle(fontSize: 14, color: Colors.green))
            ],
          ),
          Spacer(),
          Text(
            "90",
            style: TextStyle(
                fontSize: 25, fontWeight: FontWeight.bold, color: Colors.green),
          )
        ],
      ),
    );
  }
}
