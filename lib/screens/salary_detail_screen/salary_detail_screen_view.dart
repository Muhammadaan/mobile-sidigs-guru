import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:sidigsguru/widgets/button_text_custom.dart';
import './salary_detail_screen_view_model.dart';

class SalaryDetailScreenView extends SalaryDetailScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Rincian Gaji"),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.all(50),
              width: double.infinity,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "IDR 5.000.000",
                    style: TextStyle(fontSize: 26, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Confirmed",
                    style: TextStyle(color: Colors.green),
                  )
                ],
              ),
            ),
            listData(label: "Penerima", val: "Muhammad Al Fatih"),
            listData(label: "NIP", val: "723482834823"),
            listData(label: "Waktu Transaksi", val: "18 Juli 2020,14:20 WIB"),
            listData(label: "Slip Gaji", val: "Lihat Slip Gaji"),
            SizedBox(
              height: 20,
            ),
             Container(
               padding: EdgeInsets.all(20),
               child: ButtonTextCustom(
                  label: "Konfirmasi Gaji",
                  press: () {
                 
                  },
                ),
             ),
          ],
        ),
      ),
    );
  }

  Widget listData({String label, String val}) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            label,
            style: TextStyle(color: Colors.grey),
          ),
          SizedBox(
            height: 3,
          ),
          Text(
            val,
            style: TextStyle(fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }
}
