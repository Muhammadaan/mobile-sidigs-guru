import 'package:flutter/material.dart';
import 'package:sidigsguru/screens/profile_change_password/profile_change_password.dart';
import 'package:sidigsguru/screens/profile_info_school_screen/profile_info_school_screen.dart';
import 'package:sidigsguru/screens/profile_info_screen/profile_info_screen.dart';
import 'package:sidigsguru/utils/contant.dart';
import 'package:sidigsguru/widgets/loading_custom.dart';
import './profile_screen_view_model.dart';

class ProfileScreenView extends ProfileScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
      ),
      body: isLoading
          ? LoadingCustom()
          : SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(top: 20),
                child: Column(
                  children: [
                    Container(
                      height: 150,
                      width: 150,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            image: AssetImage(imgFotoGuru),
                            fit: BoxFit.cover,
                          )),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      namaGuru,
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    // Text(
                    //   "Wali Kelas : 1A",
                    //   style: TextStyle(color: Colors.grey),
                    // ),
                    menu(
                        name: "Informasi Akun",
                        ic: Icon(
                          Icons.person,
                          color: Colors.grey,
                        ),
                        ontap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ProfileInfoScreen()),
                          );
                        }),
                    menu(
                      ontap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfileChangePassword()),
                        );
                      },
                      name: "Ubah Kata Sandi",
                      ic: Icon(
                        Icons.edit,
                        color: Colors.grey,
                      ),
                    ),
                    menu(
                      name: "Bantuan",
                      ic: Icon(
                        Icons.call,
                        color: Colors.grey,
                      ),
                      ontap: () {
                        tapUrl(
                            "https://api.whatsapp.com/send/?phone=6282132901780&text&app_absent=0");
                      },
                    ),
                    menu(
                      ontap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfileInfoSchoolScreen()),
                        );
                      },
                      name: "Informasi Sekolah",
                      ic: Icon(
                        Icons.info,
                        color: Colors.grey,
                      ),
                    ),
                    menu(
                      ontap: () {
                        tapUrl("https://sidigs.com/privacy-policy");
                      },
                      name: "Kebijakan Privasi",
                      ic: Icon(
                        Icons.security,
                        color: Colors.grey,
                      ),
                    ),
                    menu(
                      ontap: () {
                        logout();
                      },
                      name: "Keluar",
                      ic: Icon(
                        Icons.exit_to_app,
                        color: Colors.grey,
                      ),
                    )
                  ],
                ),
              ),
            ),
    );
  }

  Widget menu({String name, Icon ic, Function ontap}) {
    return GestureDetector(
      onTap: () {
        ontap();
      },
      child: Container(
        padding: EdgeInsets.all(20),
        margin: EdgeInsets.only(top: 5),
        decoration: BoxDecoration(color: Colors.white),
        child: Row(
          children: [
            ic,
            SizedBox(
              width: 20,
            ),
            Text(
              name,
              style: TextStyle(color: Colors.grey),
            )
          ],
        ),
      ),
    );
  }
}
