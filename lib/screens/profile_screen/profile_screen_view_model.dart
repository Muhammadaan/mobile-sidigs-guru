import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import './profile_screen.dart';

abstract class ProfileScreenViewModel extends State<ProfileScreen> {
  // Add your state and logic here
  bool isLoading = false;
  String namaGuru = '';
  String namaSekolah = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getdata();
  }

  getdata() async {
    final sp = await SharedPreferences.getInstance();
    print("Token Sekarang");
    print(sp.getString('token'));
    //  print("Token Sekarang");

    final dataUser =
        json.decode(sp.getString('dataUser')) as Map<String, dynamic>;

    namaGuru = dataUser['nameteacher'];
    namaSekolah = dataUser['nameschool'];
    isLoading = false;
    setState(() {});
  }

  infoFiturBelumTersedia() {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: Duration(seconds: 1),
        content: const Text('Fitur Belum Tersedia'),
        backgroundColor: Colors.red,
      ),
    );
  }

  logout() async {
    setState(() {
      isLoading = true;
    });
    final sp = await SharedPreferences.getInstance();
    await sp.remove('dataUser');
    await sp.remove('token');
    await sp.remove('isLogin');

    Navigator.pushReplacementNamed(context, '/login');
  }

  Future tapUrl(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
