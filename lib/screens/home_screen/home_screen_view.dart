import 'package:flutter/material.dart';
import 'package:sidigsguru/screens/home_more_feature_screen/home_more_feature_screen.dart';
import 'package:sidigsguru/screens/salary_screen/salary_screen.dart';
import 'package:sidigsguru/utils/contant.dart';
import 'package:sidigsguru/widgets/loading_custom.dart';
import './home_screen_view_model.dart';

class HomeScreenView extends HomeScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      body: isLoading
          ? LoadingCustom()
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    Container(
                      height: 430,
                      child: Stack(
                        children: [
                          Container(
                            height: 350,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(30),
                                  bottomRight: Radius.circular(30)),
                              image: DecorationImage(
                                image: AssetImage(imgBgHome),
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                          Positioned(
                              top: 50,
                              width: MediaQuery.of(context).size.width,
                              child: Container(
                                  padding: EdgeInsets.all(20),
                                  child: Row(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "Selamat Datang",
                                        style: TextStyle(
                                          fontSize: 26,
                                          color: Colors.white,
                                        ),
                                      ),
                                      // Icon(
                                      //   Icons.home,
                                      //   color: Colors.white,
                                      // )
                                    ],
                                  ))),
                          Positioned(
                              top: 100,
                              width: MediaQuery.of(context).size.width,
                              child: Container(
                                  padding: EdgeInsets.all(20),
                                  child: Row(
                                    children: [
                                      Container(
                                        height: 70,
                                        width: 70,
                                        decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                            image: DecorationImage(
                                              image: AssetImage(imgFotoGuru),
                                              fit: BoxFit.cover,
                                            )),
                                      ),
                                      SizedBox(
                                        width: 20,
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            namaGuru,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold),
                                          ),
                                          Text(namaSekolah,
                                              style: TextStyle(
                                                  color: Colors.white)),
                                          // Text("Wali Kelas : 1A",
                                          //     style: TextStyle(
                                          //         color: Colors.white))
                                        ],
                                      )
                                    ],
                                  ))),
                          Positioned(
                            top: 200,
                            child: Container(
                              padding: EdgeInsets.all(20),
                              width: MediaQuery.of(context).size.width - 40,
                              margin: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  color: Colors.white),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      menu(
                                          label: "Presensi",
                                          icImg: iconMenuPresensi,
                                          ontap: () {
                                            onTapPresensi();
                                          }),
                                      menu(
                                          label: "Modul",
                                          icImg: iconMenuModul,
                                          ontap: () {
                                            infoFiturBelumTersedia();
                                          }),
                                      menu(
                                          label: "Media",
                                          icImg: iconMenuVc,
                                          ontap: () {
                                            infoFiturBelumTersedia();
                                          }),
                                      menu(
                                          label: "Diskusi",
                                          icImg: iconMenuDiskusi,
                                          ontap: () {
                                            infoFiturBelumTersedia();
                                          }),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      menu(
                                          label: "Jadwal",
                                          icImg: iconMenuJadwal,
                                          ontap: () {
                                            infoFiturBelumTersedia();
                                          }),
                                      menu(
                                          label: "Gaji",
                                          icImg: iconMenuGaji,
                                          ontap: () {
                                            // Navigator.push(
                                            //   context,
                                            //   MaterialPageRoute(
                                            //       builder: (context) =>
                                            //           SalaryScreen()),
                                            // );
                                          }),
                                      menu(
                                          label: "Tahfidz",
                                          icImg: iconMenuTahfidz,
                                          ontap: () {
                                            infoFiturBelumTersedia();
                                          }),
                                      menu(
                                          label: "Lainnya",
                                          icImg: iconMenuLainnya,
                                          ontap: () {
                                            infoFiturBelumTersedia();
                                            // Navigator.push(
                                            //   context,
                                            //   MaterialPageRoute(
                                            //       builder: (context) =>
                                            //           HomeMoreFeatureScreen()),
                                            // );
                                          }),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    //  Menu

                    Visibility(
                      visible: !isDevelop,
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Info Sekolah",
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "Lihat Semua",
                              style:
                                  TextStyle(fontSize: 10, color: Colors.blue),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Visibility(
                      visible: !isDevelop,
                      child: Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        margin: EdgeInsets.symmetric(horizontal: 20),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Rapat Bulanan Pegawai",
                              style: TextStyle(fontWeight: FontWeight.bold),
                            ),
                            Text(
                              "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam quis turpis morbi ut. Enim convallis pulvinar commodo egestas. ",
                              style: TextStyle(fontSize: 12),
                            )
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),

                    Visibility(
                      visible: !isDevelop,
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Today’s Schedule",
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "Lihat Semua",
                              style:
                                  TextStyle(fontSize: 10, color: Colors.blue),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),

                    // jadwal(),
                    // jadwal(),
                    // jadwal(),
                    SizedBox(
                      height: 20,
                    ),

                    Visibility(
                      visible: !isDevelop,
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Today’s converence",
                              style: TextStyle(
                                fontSize: 16,
                              ),
                            ),
                            Text(
                              "Lihat Semua",
                              style:
                                  TextStyle(fontSize: 10, color: Colors.blue),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),

                    // listVc(),
                    // listVc(),
                    // listVc()
                  ],
                ),
              ),
            ),
    );
  }

  Widget listVc() {
    return Container(
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.only(top: 10, left: 20, right: 20),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          Image.asset(
            iconMenuVc,
            width: 50,
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Bahasa Inggris",
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                "1-A | Senin,10 Maret",
                style: TextStyle(fontSize: 12, color: Colors.grey),
              ),
              Text("08.00 AM, 90 Menit",
                  style: TextStyle(fontSize: 12, color: Colors.grey))
            ],
          ),
          Spacer(),
          Container(
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
                color: mainColor, borderRadius: BorderRadius.circular(10)),
            child: Text(
              "Open Host",
              style: TextStyle(color: Colors.white, fontSize: 10),
            ),
          )
        ],
      ),
    );
  }

  Widget jadwal() {
    return Container(
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.only(top: 10, left: 20, right: 20),
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(10)),
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              child: Column(
                children: [
                  Text(
                    "07.00 WIB",
                    style: TextStyle(color: mainColor, fontSize: 16),
                  ),
                  Text(
                    "-",
                    style: TextStyle(color: mainColor, fontSize: 16),
                  ),
                  Text("08.00 WIB",
                      style: TextStyle(color: mainColor, fontSize: 16))
                ],
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
            flex: 2,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Subject :",
                    style: TextStyle(fontSize: 12, color: Colors.grey),
                  ),
                  Text(
                    "Bahasa Inggris",
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    "Kelas :",
                    style: TextStyle(fontSize: 12, color: Colors.grey),
                  ),
                  Text(
                    "1A",
                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget menu({String label, String icImg, Function ontap}) {
    return GestureDetector(
      onTap: () {
        ontap();
      },
      child: Container(
        width: 70,
        child: Column(
          children: [
            // SvgPicture.asset(iconMenuPresensi),
            Image.asset(
              icImg,
              width: 55,
            ),
            Text(
              label,
              style: TextStyle(fontSize: 12),
            )
          ],
        ),
      ),
    );
  }
}
