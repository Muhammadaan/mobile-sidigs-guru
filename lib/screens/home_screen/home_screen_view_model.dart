import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sidigsguru/api/data_api.dart';
import 'package:sidigsguru/screens/presence_screen/presence_screen.dart';
import './home_screen.dart';

abstract class HomeScreenViewModel extends State<HomeScreen> {
  // Add your state and logic here
  bool isDevelop = true;
  String namaGuru = '';
  String namaSekolah = '';
  bool isLoading = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getdata();
  }

  getdata() async {
    final sp = await SharedPreferences.getInstance();

    var data = await ApiData.getRefreshtoken(token: sp.getString('token'));
    var jsonObject = json.decode(data.toString());
    sp.setString('token', jsonObject['data']['access_token']);
    print(jsonObject);

    final dataUser =
        json.decode(sp.getString('dataUser')) as Map<String, dynamic>;

    namaGuru = dataUser['nameteacher'];
    namaSekolah = dataUser['nameschool'];
    isLoading = false;
    setState(() {});
  }

  onTapPresensi() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PresenceScreen()),
    );
  }

  infoFiturBelumTersedia() {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        duration: Duration(seconds: 1),
        content: const Text('Fitur Belum Tersedia'),
        backgroundColor: Colors.red,
      ),
    );
  }
}
