import 'package:flutter/material.dart';
import 'package:sidigsguru/utils/contant.dart';
import 'package:sidigsguru/widgets/wait_update_screen.dart';
import './chat_screen_view_model.dart';

class ChatScreenView extends ChatScreenViewModel {
  @override
  Widget build(BuildContext context) {
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Chat"),
      ),
      body: isDevelop
          ? WaitUpdateScreen()
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    listChat(),
                    listChat(),
                    listChat(),
                    listChat(),
                    listChat(),
                    listChat(),
                    listChat(),
                    listChat()
                  ],
                ),
              ),
            ),
    );
  }

  Container listChat() {
    return Container(
      padding: EdgeInsets.all(20),
      margin: EdgeInsets.only(top: 5),
      decoration: BoxDecoration(color: Colors.white),
      child: Row(
        children: [
          Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: AssetImage(imgFotoSiswa),
                  fit: BoxFit.cover,
                )),
          ),
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Alif Salman",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
                Text(
                  "Halo Selamat Pagi,Saya Mau Tanya .. ",
                  style: TextStyle(color: Colors.grey, fontSize: 12),
                )
              ],
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "11:11",
                style: TextStyle(color: Colors.grey),
              ),
              Container(
                  padding: EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: Colors.green, shape: BoxShape.circle),
                  child: Text(
                    "1",
                    style: TextStyle(color: Colors.white, fontSize: 10),
                  ))
            ],
          ),
        ],
      ),
    );
  }
}
