import 'package:flutter/material.dart';
import './presence_submit_data_view.dart';

class PresenceSubmitData extends StatefulWidget {
  final String title;

  const PresenceSubmitData({Key key, this.title}) : super(key: key);
  
  @override
  PresenceSubmitDataView createState() => new PresenceSubmitDataView();
}
  
