import 'package:flutter/material.dart';
import 'package:sidigsguru/utils/contant.dart';
import 'package:sidigsguru/widgets/button_text_custom.dart';
import './presence_submit_data_view_model.dart';

class PresenceSubmitDataView extends PresenceSubmitDataViewModel {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    // Replace this with your build function
    return Scaffold(
      appBar: AppBar(
        title: Text("Presensi ${widget.title}"),
      ),
      body: isAbsen
          ? Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.check_circle_outline,
                    size: 200,
                  ),
                  Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ButtonTextCustom(
                          press: () {
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                '/menu', (Route<dynamic> route) => false);
                          },
                          isEnable: true,
                          label: "Kembali Ke Baranda"),
                    ),
                  )
                ],
              ),
            )
          : Container(
              padding: EdgeInsets.all(15),
              child: Column(
                children: [
                  fileImage == null
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(Icons.info),
                            SizedBox(
                              height: 10,
                            ),
                            Text('Anda Belum Ambil Foto'),
                          ],
                        )
                      : Container(
                          child: Image.file(fileImage,
                              width: width, height: 300, fit: BoxFit.cover)),
                  SizedBox(
                    height: 15,
                  ),
                  Visibility(
                    visible: fileImage != null,
                    child: ButtonTextCustom(
                      isEnable: true,
                      label: "Simpan Absensi",
                      clr: mainColor,
                      press: () {
                        submitAbsensi();
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  ButtonTextCustom(
                    isEnable: true,
                    label:
                        fileImage == null ? "Ambil Foto" : "Ambil Foto Ulang",
                    clr: fileImage == null ? mainColor : Colors.blue[300],
                    press: () {
                      getImage();
                    },
                  ),
                ],
              ),
            ),
    );
  }
}
