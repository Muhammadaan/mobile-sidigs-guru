import 'package:flutter/material.dart';
import 'package:sidigsguru/utils/contant.dart';

class ButtonTextCustom extends StatelessWidget {
  final String label;
  final Color clr;
  final Function press;
  final bool isEnable;

  const ButtonTextCustom(
      {Key key, this.label, this.clr = mainColor, this.press, this.isEnable})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: isEnable ? press : null,
      child: Text(
        label,
        style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
      ),
      style: TextButton.styleFrom(
          primary: Colors.white,
          backgroundColor: isEnable ? clr : Colors.grey,
          onSurface: Colors.white,
          minimumSize: Size(double.infinity, 50)),
    );
  }
}
