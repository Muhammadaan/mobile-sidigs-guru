import 'package:flutter/material.dart';

class WaitUpdateScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 100,
          ),
          Image.asset(
            "assets/img/TungguUpdate.png",
            width: 300,
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Fitur belum tersedia",
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            "Tunggu update versi terbaru kita ya",
          ),
        ],
      ),
    );
  }
}
