import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:sidigsguru/utils/app_provider.dart';
import 'package:sidigsguru/utils/contant.dart';
import 'package:sidigsguru/utils/route.dart';

void main() async {
  runApp(
    MultiProvider(
      providers: appprovider,
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          fontFamily: 'Poppins-Regular',
          primarySwatch: Colors.blue,
          appBarTheme: AppBarTheme(color: mainColor)),
      onGenerateRoute: AppRoute.generateRoute,
      initialRoute: '/',
    );
  }
}
